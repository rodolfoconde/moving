@extends('layout.login')
@section('content')
<div class="container">
    <div class="card card-login mx-auto mt-5">
        <div class="card-header">Acesso</div>
        <div class="card-body">
            <form class="form-horizontal">
                <div class="form-group">
                    <label for="email">E-mail</label>
                    <input class="form-control" id="email" type="email" aria-describedby="emailHelp" placeholder="Enter email">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input class="form-control" id="pass" type="password">
                </div>
                <button type="submit" class="btn btn-primary btn-block">Acessar</button>
            </form>
            <div class="text-center">
                <a class="d-block small mt-3" href="register.html">Registrar uma conta</a>
                <a class="d-block small" href="forgot-password.html">Esqueci a senha</a>
            </div>
        </div>
    </div>
@endsection

@section('script')

<script type="application/javascript">

    $(document).ready(function () {

        $('form').on('submit',function(e) {
            e.preventDefault();


            $.ajax({
                url: '/api/login',
                method: 'POST',
                dataType: 'json',
                data: {
                    // hash: hash,
                    email: $('#email').val(),
                    password: $('#pass').val()
                },
                success: function (r) {
                    $.notify({
                        message: r.content
                    }, {
                        type: 'success',
                        timer: 4000
                    });
                    setTimeout(function () {
                        window.location = host + '/login';
                    }, 2000);
                },
                error: function (jqXHR) {
                    $.notify({
                        message: jqXHR.responseJSON.content
                    }, {
                        type: 'danger',
                        timer: 4000
                    });
                }
            });
        });

    });

</script>

@endsection