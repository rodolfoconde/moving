@extends('layout.app')
@section('content')
    <h1>Imóveis</h1>
    @include('components.messages')

    <form id="form">
        <div class="form-group row">
            <label for="example-text-input" class="col-2 col-form-label">Imobiliaria</label>
            <div class="col-10">
                <select name="type" class="form-control">
                    @foreach($imobiliarias as $imobiliaria)
                        <option value="{{$imobiliaria->id}}" @if($imobiliaria->id == $imovel->imobiliaria_id) selected @endif>{{$imobiliaria->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="example-search-input" class="col-2 col-form-label">Tipo</label>
            <div class="col-10">
                <select name="type" id="type" class="form-control">
                    <option value="1" @if($imovel->type == "Casa") selected @endif>Casa</option>
                    <option value="2" @if($imovel->type == "Apartamento") selected @endif>Apartamento</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="description" class="col-2 col-form-label">Descrição</label>
            <div class="col-10">
                <input class="form-control" type="text" value="{{$imovel->description}}" name="description" id="description">
            </div>
        </div>
        <div class="form-group row">
            <label for="address" class="col-2 col-form-label">Endereço</label>
            <div class="col-10">
                <input class="form-control" type="text" value="{{$imovel->address}}"  name="address" id="address">
            </div>
        </div>
        <div class="form-group row pull-right">
            <input type="hidden" name="id" value="{{$imovel->id}}">
            <a href="/home" class="btn btn-primary padding-btn">Voltar</a>
            <a href="#" data-id="{{$imovel->id}}" class="delete btn btn-danger padding-btn">Deletar Imóvel</a>
            <button type="submit" class="btn btn-success padding-btn">Salvar</button>
        </div>
    </form>


@endsection
@section('script')

    <script>
        $(document).ready(function () {


            $('#form button').on('click', function (e) {
                $('.alert').html('').hide();
                e.preventDefault();
                $.ajax({
                    url: '/app/imoveis/update',
                    method: 'POST',
                    dataType: 'json',
                    data: $('#form').serialize(),
                    success: function (r) {
                        if(r.content){
                            $('.alert-success').html(r.message).show();
                        }else{
                            $('.alert-danger').html(r.message).show();
                        }
                    },
                    error: function (jqXHR) {
                        $('.alert-danger').html('Opps, ocorreu um erro em nosso sistema').show();
                    }
                });
            })


            $('.delete').on('click', function (e) {
                e.preventDefault();
                $.ajax({
                    url: '/app/imoveis/delete/' + $(this).attr('data-id'),
                    method: 'GET',
                    dataType: 'json',
                    success: function (r) {
                        if(r.content){
                            $('.alert-success').html(r.message).show();
                            window.location.href="/home";
                        }else{
                            $('.alert-danger').html(r.message).show();
                        }
                    },
                    error: function (jqXHR) {
                        $('.alert-danger').html('Opps, ocorreu um erro em nosso sistema').show();
                    }
                });
            })


        });
    </script>


@endsection
