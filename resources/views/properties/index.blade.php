@extends('layout.app')
@section('content')
    <h1>Imóveis</h1>
    @include('components.messages')
    <a class="btn btn-primary pull-right" href="/home/create">Criar imóvel</a>
    <label>Filtro: </label>
    <select id='imobiliaria' class="">
    </select>
    <table class="table" id="imoveis">
        <thead>
            <th>ID</th>
            <th>Type</th>
            <th>Description</th>
            <th>Address</th>
        </thead>
        <tbody>

        </tbody>

    </table>

@endsection
@section('script')


    <script type="application/javascript">


        $(document).ready(function () {

            var html = '';
            $.ajax({
                url: '/app/imobiliarias/list',
                method: 'GET',
                dataType: 'json',
                success: function (r) {
                    var data = r.content;
                    for( i in data){
                        html += '<option value="'+ data[i].id +'">';
                        html += data[i].name;
                        html += '</option>';
                    }


                    $('#imobiliaria').html(html);
                    loadImoveis();
                },
                error: function (jqXHR) {
                    console.log(jqXHR);
                }
            });

            $('#imobiliaria').on('change', function () {
                loadImoveis();
            });

            function loadImoveis() {
                var html = '';
                $.ajax({
                    url: '/app/imoveis/list',
                    method: 'POST',
                    data: {
                        'imobiliaria': $('#imobiliaria').val()
                    },
                    dataType: 'json',
                    success: function (r) {
                        var data = r.content;
                        for( i in data){
                            html += '<tr>';
                            html += '<td><a href="/home/'+ data[i].id +'/edit"> '+ data[i].id +'</a></td>';
                            html += '<td>'+ data[i].type +'</td>';
                            html += '<td>'+ data[i].description +'</td>';
                            html += '<td>'+ data[i].address +'</td>';
                            html += '</tr>';
                        }

                        $('#imoveis tbody').html(html);
                    },
                    error: function (jqXHR) {
                        console.log(jqXHR);
                    }
                });
            }


            });



    </script>



@endsection
