@extends('layout.app')
@section('content')
    <h1>Imóveis</h1>
    <form id="form">

        @include('components.messages')

    <div class="form-group row">
        <label for="example-text-input" class="col-2 col-form-label">Imobiliaria</label>
        <div class="col-10">
            <select name="imobiliaria" class="form-control">
                @foreach($imobiliarias as $imobiliaria)
                <option value="{{$imobiliaria->id}}">{{$imobiliaria->name}}</option>
                    @endforeach
                </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="example-search-input" class="col-2 col-form-label">Tipo</label>
        <div class="col-10">
            <select name="type" class="form-control">
                <option value="1">Casa</option>
                <option value="2">Apartamento</option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="description" class="col-2 col-form-label">Descrição</label>
        <div class="col-10">
            <input class="form-control" type="text" value="" name="description" id="description">
        </div>
    </div>
    <div class="form-group row">
        <label for="address" class="col-2 col-form-label">Endereço</label>
        <div class="col-10">
            <input class="form-control" type="text" value=""  name="address" id="address">
        </div>
    </div>
        <div class="form-group row pull-right">
            <a href="/home" class="btn btn-primary">Voltar</a>
            <button type="submit" class="btn btn-primary">Salvar</button>
        </div>
    </form>
@endsection
@section('script')

    <script type="application/javascript">


        $(document).ready(function () {


            $('#form button').on('click', function (e) {
                $('.alert').html('').hide();
                e.preventDefault();
                $.ajax({
                    url: '/app/imoveis/create',
                    method: 'POST',
                    dataType: 'json',
                    data: $('#form').serialize(),
                    success: function (r) {
                        if(r.content){
                           $('.alert-success').html(r.message).show();
                        }else{
                            $('.alert-danger').html(r.message).show();
                        }
                    },
                    error: function (jqXHR) {
                        $('.alert-danger').html('Opps, ocorreu um erro em nosso sistema').show();
                    }
                });
            })


        });



    </script>




@endsection
