<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'API\AuthController@login');
Route::post('login/refresh', 'API\AuthController@login');
Route::post('logout', 'API\AuthController@login');

Route::get('/users/forgot-password', 'API\UsersServicesController@forgotPassword');
Route::get('/users/register', 'API\UsersServicesController@store');

Route::group(['middleware' => 'auth:api'], function(){
    Route::post('/imoveis/filter', 'API\ImoveisServicesController@filter');
    Route::resource('/imoveis', 'API\ImoveisServicesController');
    Route::get('/imobiliaria', 'API\ImobiliariaServicesController@index');
});
