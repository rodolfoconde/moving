<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', function () {
    return view('auth.login');
});

//Route::group(['middleware' => 'auth:api'], function() {
    Route::resource('/home', 'AdminController');
  //  Route::get('/imoveis', 'AdminController@index');
//});


/*
 * Serviços
 *
 * */

Route::group(['prefix' => 'app'], function() {
    Route::post('/imoveis/list', 'ImovelController@index');
    Route::post('/imoveis/create', 'ImovelController@store');
    Route::post('/imoveis/update', 'ImovelController@update');
    Route::get('/imoveis/delete/{id}', 'ImovelController@delete');

    Route::get('/imobiliarias/list', 'ImobiliariaController@index');
});

