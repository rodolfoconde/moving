<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableImoveis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imoveis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('imobiliaria_id')->unsigned();
            $table->enum('type', ['Casa', 'Apartamento']);
            $table->text('description');
            $table->string('address');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('imobiliaria_id')->references('id')->on('imobiliarias');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
