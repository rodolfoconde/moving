<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Imobiliaria;

class ImobiliariaController extends Controller
{
    private $api = '';
    public function __construct(){

        $this->api = new Imobiliaria();

    }

    public function index(){
        return response()->json($this->api->imobiliariaList());
    }

}
