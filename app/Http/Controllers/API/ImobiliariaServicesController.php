<?php

namespace App\Http\Controllers\API;

use App\Imobiliaria;
use App\Imovel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImobiliariaServicesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $imobiliaria    = Imobiliaria::all();

        if(!$imobiliaria) {
            return response()->json([
                'content'   => '',
                'message'   => 'Record not found',
            ], 404);
        }

        return response()->json(['content' => $imobiliaria, 'message'=>'']);

    }

}
