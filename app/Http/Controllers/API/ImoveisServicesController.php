<?php

namespace App\Http\Controllers\API;

use App\Imovel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImoveisServicesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $imoveis    = Imovel::with('imobiliaria')->get();

        if(!$imoveis) {
            return response()->json([
                'content'   => '',
                'message'   => 'Record not found',
            ], 404);
        }

        return response()->json(['content' => $imoveis, 'message'=>'']);

    }

    public function filter(Request $request)
    {

        $imoveis    = Imovel::with('imobiliaria')->where('imobiliaria_id', '=', $request->id)->get();

        if(!$imoveis) {
            return response()->json([
                'content'   => '',
                'message'   => 'Record not found',
            ], 404);
        }

        return response()->json(['content' => $imoveis, 'message'=>'']);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        try{

        $imoveis   = new Imovel();
        $imoveis->fill($data);
        $imoveis->save();
        }catch (\Exception $e){
            return response()->json(['content' => '', 'message'=>'Erro ao cadastrar imóvel.']);
        }

        return response()->json(['content' => $imoveis, 'message'=>'Cadastrado com sucesso!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $imoveis    = Imovel::with('imobiliaria')->where('id', $id)->first();

        if(!$imoveis) {
            return response()->json([
                'content'   => '',
                'message'   => 'Record not found',
            ], 404);
        }

        return response()->json(['content' => $imoveis, 'message'=>'']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $imoveis    = Imovel::where('id', $id)->first();
        if(!$imoveis) {
            return response()->json([
                'content'   => '',
                'message'   => 'Record not found',
            ], 404);
        }

        $imoveis->fill($request->all());
        $imoveis->save();
        return response()->json(['content' => $imoveis, 'message'=>'Alterado com sucesso!'], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $imoveis    = Imovel::where('id','=', $id)->first();

        if(!$imoveis) {
            return response()->json([
                'content'   => '',
                'message'   => 'Record not found',
            ], 404);
        }

        $imoveis->delete();

        return response()->json(['content' => $imoveis, 'message'=>''], 201);
    }
}
