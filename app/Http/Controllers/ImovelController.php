<?php

namespace App\Http\Controllers;

use App\Services\Imoveis;
use Illuminate\Http\Request;

class ImovelController extends Controller
{
    private $api = '';
    public function __construct(){

        $this->api = new Imoveis();

    }

    public function index(Request $request){
        $prepare    = [
            'id'    => $request->imobiliaria
        ];

        return response()->json($this->api->imovelList($prepare));
    }

    public function store(Request $request){

        $prepare    = [
          'imobiliaria_id'    => $request->imobiliaria,
          'type'              => $request->type,
          'description'       => $request->description,
          'address'           => $request->address,
        ];

        return response()->json($this->api->imovelCreate($prepare));
    }


    public function update(Request $request){

        $prepare    = [
            'imobiliaria_id'    => $request->imobiliaria,
            'type'              => $request->type,
            'description'       => $request->description,
            'address'           => $request->address,
            'id'                => $request->id

        ];
        return response()->json($this->api->imovelUpdate($request->id, $prepare));
    }

    public function delete($id){
        return response()->json($this->api->imovelDelete($id));
    }
}
