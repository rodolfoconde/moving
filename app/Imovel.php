<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Imovel extends Model
{
    protected $table    = 'imoveis';
    protected $fillable = ['imobiliaria_id', 'type', 'description', 'address'] ;
    protected $dates    = ['deleted_at'];

    public function imobiliaria(){
        return $this->belongsTo('App\Imobiliaria');
    }

}
