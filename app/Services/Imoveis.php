<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;


class Imoveis
{
    private $url = '';

    public function __construct() {
        $this->url = \Config::get('app.url');
    }

    public function imovelList($request)
    {
        try {
            var_dump(Session::all());
           // dd(\Session::get('access_token'));
            $client = new Client();

            $response = $client->request('POST', $this->url . 'api/imoveis/filter', [
                'headers' => [
                    'Authorization' => 'Bearer '. Auth::user()->getRememberToken(),
                    'Accept'        => 'application/json'
                ],
                'form_params' => $request,
            ]);

            return json_decode($response->getBody()->getContents());
        } catch (\GuzzleHttp\Exception\ClientException  $e) {
            echo $e->getMessage();
            $response = new \StdClass();
            $response->message = 'Btw-nr niet actief of bestaat niet';
            $response->content = null;
            $response->code = 503;
            return $response;
        }  catch (\Exception $e) {
            echo $e->getMessage();
            $response = new \StdClass();
            $response->message = 'Unable to access API';
            $response->content = null;
            $response->error = 503;

            return $response;
        }
    }

    public function imovelCreate($request)
    {
        try {

            $client = new Client();
            $response = $client->request('POST', $this->url . 'api/imoveis', [
                'form_params' => $request,
            ]);
            return json_decode($response->getBody()->getContents());
        } catch (\GuzzleHttp\Exception\ClientException  $e) {
            $response = new \StdClass();
            $response->message = 'Btw-nr niet actief of bestaat niet';
            $response->content = null;
            $response->code = 503;
            return $response;
        }  catch (\Exception $e) {
            $response = new \StdClass();
            $response->message = 'Unable to access API';
            $response->content = null;
            $response->error = 503;

            return $response;
        }
    }

    public function imovelUpdate($id, $request)
    {
        try {
            $client = new Client();

            $response = $client->request('PUT', $this->url . 'api/imoveis/' . $id, [
                'form_params' => $request,
            ]);

            return json_decode($response->getBody()->getContents());
        } catch (\GuzzleHttp\Exception\ClientException  $e) {
            echo $e->getMessage();
            $response = new \StdClass();
            $response->message = 'Btw-nr niet actief of bestaat niet';
            $response->content = null;
            $response->code = 503;
            return $response;
        }  catch (\Exception $e) {
            $response = new \StdClass();
            $response->message = 'Unable to access API';
            $response->content = null;
            $response->error = 503;

            return $response;
        }
    }

    public function imovelDelete($id)
    {
        try {
            $client = new Client();
            $response = $client->request('DELETE', $this->url . 'api/imoveis/' . $id);
            return json_decode($response->getBody()->getContents());
        } catch (\GuzzleHttp\Exception\ClientException  $e) {

            $response = new \StdClass();
            $response->message = 'Btw-nr niet actief of bestaat niet';
            $response->content = null;
            $response->code = 503;
            return $response;
        }  catch (\Exception $e) {
            $response = new \StdClass();
            $response->message = 'Unable to access API';
            $response->content = null;
            $response->error = 503;

            return $response;
        }
    }


}
