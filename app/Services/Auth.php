<?php

namespace App\Services;

use App\User;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;


class Auth
{
    private $url = '';

    public function __construct() {
        $this->url = \Config::get('app.url');
    }

    public function authenticate($request)
    {
        try {
            $client = new Client();

            $response = $client->request('POST', $this->url . 'oauth/token', [
                'form_params' => [
                    'client_id' => 2,
                    // The secret generated when you ran: php artisan passport:install
                    'client_secret' => 'HqP12jfLuKqox5o5ObWSkfmmIzRyAtsE2hUYYYXJ',
                    'grant_type' => 'password',
                    'username' => $request['email'],
                    'password' => $request['password'],
                    'scope' => '*',
                ]
                ]);

            $auth = json_decode((string) $response->getBody());
           // \Session::put('access_token', $auth->access_token);
           // \Illuminate\Support\Facades\Auth::user()->remeber_token = $auth->access_token);
            return response()->json(['content' => $auth->access_token, 'message'=>'Autenticado com sucesso.']);
        } catch (\GuzzleHttp\Exception\ClientException  $e) {

            return response()->json(['content' => '', 'message'=>'Unauthorized'], 401);

        } catch (\Exception $e) {
            $response = new \StdClass();
            $response->message = 'Unable to access API';
            $response->content = null;
            $response->error = 503;

            return $response;
        }
    }



}
