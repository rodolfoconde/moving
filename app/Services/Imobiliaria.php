<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;


class Imobiliaria
{
    private $url = '';

    public function __construct() {
        $this->url = \Config::get('app.url');
    }

    public function imobiliariaList()
    {
        try {
            $client = new Client();

            $response = $client->request('GET', $this->url . 'api/imobiliaria',
                [
                    'headers' => [
                        'Accept' => 'application/json',
                        'Authorization' => 'Bearer '.$accessToken,
                    ],
                ]);

            return json_decode($response->getBody()->getContents());
        } catch (\GuzzleHttp\Exception\ClientException  $e) {
            $response = new \StdClass();
            $response->message = 'Btw-nr niet actief of bestaat niet';
            $response->content = null;
            $response->code = 503;
            return $response;
        }  catch (\Exception $e) {
            $response = new \StdClass();
            $response->message = 'Unable to access API';
            $response->content = null;
            $response->error = 503;

            return $response;
        }
    }



}
